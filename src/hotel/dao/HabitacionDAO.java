/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.dao;

import hotel.entities.Habitacion;
import hotel.entities.MiError;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class HabitacionDAO {

    /**
     * cargar cargar las habitaciones
     * @return linkedlist con las habitaciones cargadas
     */
    public LinkedList<Habitacion> cargarTodosActivos() {
        LinkedList<Habitacion> habitacion = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from habitacion order by id asc;";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                habitacion.add(cargarHabitacion(rs));
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar las habitaciones, favor intente nuevamente");
        }

        return habitacion;
    }
/**
 * cargar habitaciones con filtro
 * @param fecha filtro para las habitaciones
 * @return linkedlist con las habitaciones filtradas
 */
    public LinkedList<Habitacion> cargarFiltro(String fecha) {
        LinkedList<Habitacion> habitaciones = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from habitacion where fecha_salida like ? ";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, fecha);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                habitaciones.add(cargarHabitacion(rs));
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar las habitaciones, favor intente nuevamente");
        }

        return habitaciones;
    }

    /**
     * cargar la habitacion al sistema
     * @param rs resultset
     * @return habitacion cargada
     * @throws SQLException 
     */
    private Habitacion cargarHabitacion(ResultSet rs) throws SQLException {
        Habitacion h = new Habitacion();
        h.setId(rs.getInt("id"));
        h.setNumero(rs.getString("numero"));
        h.setEstado(rs.getBoolean("estado_habitacion"));
        h.setFechaSalida(rs.getString("fecha_salida"));

        //Cargar la foranea
        TipoHabitacionDAO tdao = new TipoHabitacionDAO();
        h.setTipo(tdao.cargarID(rs.getInt("tip_hab")));

        h.setActivo(rs.getBoolean("activo"));
        return h;
    }

    /**
     * cargar la habitacion por id
     * @param id de la habitacion
     * @return habitacion cargada por id
     */
    public Habitacion cargarID(int id) {
        Habitacion habitacion = null;

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from habitacion where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                habitacion = cargarHabitacion(rs);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar las habitaciones, favor intente nuevamente");
        }

        return habitacion;
    }

    /**
     * insercion de la habitacion
     * @param h habitacion que se va a insertar
     * @return true o false si la habitacion se inserto
     */
    public boolean insertar(Habitacion h) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into habitacion(numero,tip_hab)\n"
                    + "values(?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, h.getNumero());
            stmt.setInt(2, h.getTipo().getId());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo registrar la habitacion, favor intente nuevamente");
        }
    }
/**
 * ocupar la habitacion
 * @param habitacion que se va a a ocupar
 * @return true o false si la habitacion ocupada
 */
    public boolean ocupar(Habitacion habitacion) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update habitacion set estado_habitacion = ?, fecha_salida = ? where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setBoolean(1, habitacion.isEstado());
            stmt.setString(2, habitacion.getFechaSalida());
            stmt.setInt(3, habitacion.getId());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo ocupar la habitacion, favor intente nuevamente");
        }
    }

    /**
     * desocupar la habitacion
     * @param habitacion habitacion que se a a desocupar
     * @return true o false si la habitacion se pudo desocupar
     */
    public boolean desocupar(Habitacion habitacion) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update habitacion set estado_habitacion = ?, fecha_salida = ? where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setBoolean(1, habitacion.isEstado());
            stmt.setString(2, habitacion.getFechaSalida());
            stmt.setInt(3, habitacion.getId());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo ocupar la habitacion, favor intente nuevamente");
        }
    }
}
