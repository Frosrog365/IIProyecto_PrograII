/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.dao;

import hotel.entities.MiError;
import hotel.entities.TipoHabitacion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class TipoHabitacionDAO {

    /**
     * cargar tipos de habitaciones
     * @param activo true o false para cargar los tipos
     * @return linked list con los tipos de habitacion
     */
    public LinkedList<TipoHabitacion> selectAll(boolean activo) {
        LinkedList<TipoHabitacion> tipos = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = activo ? "select * from tipo_habitacion where activo = true"
                    : "select * from tipo_habitacion ";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                tipos.add(cargarTipos(rs));
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar los tipos de habitacion, favor intente nuevamente");
        }

        return tipos;
    }

    /**
     * cargar tipos de habitacion por ID
     * @param id de la habitacion a cargar
     * @return tipo de habitacion cargado
     */
    public TipoHabitacion cargarID(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from tipo_habitacion where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarTipos(rs);
            }
        } catch (Exception ex) {

            throw new MiError("Problemas al cargar los tipos de habitacion, favor intente nuevamente");
        }
        return null;
    }
/**
 * cargar todos los tipos de habitacion
 * @param rs resultset
 * @return tipo de habitacion cargado
 * @throws SQLException 
 */
    private TipoHabitacion cargarTipos(ResultSet rs) throws SQLException {
        TipoHabitacion tipo = new TipoHabitacion();
        tipo.setId(rs.getInt("id"));
        tipo.setTipo(rs.getString("tipo_habitacion"));
        return tipo;
    }

    /**
     * insertar tipos de habitaicion
     * @param t tipo de habitacion
     * @return true o false si el tipo de pudo registrar
     */
    public boolean insertar(TipoHabitacion t) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into tipo_habitacion(tipo_habitacion)"
                    + "values(?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, t.getTipo());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo registrar el tipo de habitacion, favor intente nuevamente");
        }
    }

}
