/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.dao;

import hotel.entities.MiError;
import hotel.entities.Reservacion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class ReservacionDAO {

    /**
     * insertar reservacion
     *
     * @param reservacion que se va a registrar
     * @return true o false si se registra
     */
    public boolean insertar(Reservacion reservacion) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into reservacion(id_cli,dias,desayuno,fecha_ent,fecha_sal,id_hab)\n"
                    + "values(?,?,?,?,?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, reservacion.getCliente().getId());
            stmt.setInt(2, reservacion.getDias());
            stmt.setInt(3, reservacion.getDesayuno());
            stmt.setString(4, reservacion.getFechaEnt());
            stmt.setString(5, reservacion.getFechaSal());
            stmt.setInt(6, reservacion.getHabitacion().getId());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new MiError("No se pudo registrar la reservacion, favor intente nuevamente");
        }
    }

    /**
     * cargar todas las reservaciones activas
     *
     * @return linkedlist con todas las reservaciones
     */
    public LinkedList<Reservacion> cargarTodosActivos() {
        LinkedList<Reservacion> reservaciones = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from reservacion where activo = true";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                reservaciones.add(cargarReservacion(rs));
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar las reservaciones, favor intente nuevamente");
        }

        return reservaciones;
    }

    /**
     * cargar reservacion por filtro
     *
     * @param filtro
     * @return linkedlist con reservaciones filtradas
     */
    public LinkedList<Reservacion> cargarFiltro(String filtro) {
        LinkedList<Reservacion> reservaciones = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from reservacion,cliente where (cliente.cedula like ? "
                    + "or fecha_ent like ? or fecha_sal like ? ) and cliente.id = id_cli and reservacion.activo = true";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, filtro + '%');
            stmt.setString(2, filtro + '%');
            stmt.setString(3, filtro + '%');
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                reservaciones.add(cargarReservacion(rs));
            }
        } catch (Exception ex) {

            throw new MiError("Problemas al cargar las reservaciones, favor intente nuevamente");
        }

        return reservaciones;
    }

    /**
     * cargar reservaciones por id
     *
     * @param id de la reservacion
     * @return reservacion cargada
     */
    public Reservacion cargarID(int id) {
        Reservacion r = null;

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from reservacion where id = ? and activo = true";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                r = cargarReservacion(rs);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar las reservaciones, favor intente nuevamente");
        }

        return r;
    }

    /**
     * cargar reservacion al sistema
     *
     * @param rs resultset
     * @return reservacion cargada al sistema
     * @throws SQLException
     */
    private Reservacion cargarReservacion(ResultSet rs) throws SQLException {
        Reservacion r = new Reservacion();
        r.setId(rs.getInt("id"));
        r.setDesayuno(rs.getInt("desayuno"));
        r.setDias(rs.getInt("dias"));
        r.setFechaEnt(rs.getString("fecha_ent"));
        r.setFechaSal(rs.getString("fecha_sal"));

        //Cargar la foranea
        ClienteDAO pdao = new ClienteDAO();
        r.setCliente(pdao.cargarID(rs.getInt("id_cli")));
        HabitacionDAO hdao = new HabitacionDAO();
        r.setHabitacion(hdao.cargarID(rs.getInt("id_hab")));

        r.setActivo(rs.getBoolean("activo"));
        return r;
    }

    /**
     * desactivar reservacion
     *
     * @param id id de la reservacion a desactivar
     * @return true o false si la reservacion está desactivada
     */
    public boolean borrar(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update reservacion set activo = false where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo eliminar la reservacion, favor intente nuevamente");
        }
    }
    /**
     * editar la reservacion
     * @param r
     * @return true o false si se pudo editar la reservacion
     */
    public boolean editar(Reservacion r){
        try (Connection con = Conexion.getConexion()) {
            String sql = "update reservacion set dias = ?, desayuno = ? , fecha_ent = ?"
                    + ", fecha_sal =? where id = ?;"
                    + "update habitacion set fecha_salida = ? where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, r.getDias());
            stmt.setInt(2, r.getDesayuno());
            stmt.setString(3, r.getFechaEnt());
            stmt.setString(4, r.getFechaSal());
            stmt.setInt(5, r.getId());
            stmt.setString(6, r.getHabitacion().getFechaSalida());
            stmt.setInt(7, r.getHabitacion().getId());
            
            
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo modificar la reservacion, favor intente nuevamente");
        }
    }

}
