/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.dao;

import hotel.entities.Desayuno;
import hotel.entities.MiError;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Fabio_2
 */
public class DesayunoDAO {

    /**
     * insertar desayuno
     *
     * @param d desayuno que se va insertar
     * @return true o false si el desayuno se insertó
     */
    public boolean insertar(Desayuno d) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into desayuno(fecha)\n"
                    + "values(?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, d.getFecha());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo registrar el desayuno, favor intente nuevamente");
        }
    }

    /**
     * metodo para sacar la cantidad de desayunos
     *
     * @param cantidadDesayunos int a editar
     * @param fecha fecha filtro
     * @return cantidad de desayunos
     * @throws SQLException
     */
    public int cantidadDesayunos(int cantidadDesayunos, String fecha) throws SQLException {
        try (Connection con = Conexion.getConexion()) {
            String sql = "Select count(*) from desayuno where fecha = ? and activo = true";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, fecha);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cantidadDesayunos = rs.getInt("count");
            }
        } catch (Exception ex) {
            throw new MiError("Problema a la hora de ver los desayunos");
        }
        return 0;
    }
/**
 * Metodo para desactivar desayunos
 * @param fecha
 * @return 
 */
    public boolean borrar(String fecha) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update desayuno set activo = false\n"
                    + "where fecha = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, fecha);
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo registrar el desayuno, favor intente nuevamente");
        }
    }
}
