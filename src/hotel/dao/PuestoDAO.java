/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.dao;

import hotel.entities.MiError;
import hotel.entities.Puesto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class PuestoDAO {

    /**
     * cargar los puestos
     * @param activo true o false si está activo
     * @return los puestos activos
     */
    public LinkedList<Puesto> selectAll(boolean activo) {
        LinkedList<Puesto> puestos = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = activo ? "select * from puesto where activo = true"
                    : "select * from puesto ";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                puestos.add(cargarPuestos(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar las razas, favor intente nuevamente");
        }

        return puestos;
    }
/**
 * cargar puestos al sistema
 * @param rs resultset
 * @return puesto cargado
 * @throws SQLException 
 */
    private Puesto cargarPuestos(ResultSet rs) throws SQLException {
        Puesto puesto = new Puesto();
        puesto.setId(rs.getInt("id"));
        puesto.setPuesto(rs.getString("puesto"));
        puesto.setActivo(rs.getBoolean("activo"));
        return puesto;
    }
/**
 * cargar puesto por id
 * @param id del puesto
 * @return puesto cargado por id
 */
    public Puesto cargarID(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from puesto where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarPuestos(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los puestos, favor intente nuevamente");
        }
        return null;
    }
/**
 * insertar puestos
 * @param p que se va a insertar
 * @return true o false si el puesto se va a insertar
 */
    public boolean insertar(Puesto p) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into puesto(puesto)"
                    + "values(?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, p.getPuesto());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo registrar el puesto, favor intente nuevamente");
        }
    }
}
