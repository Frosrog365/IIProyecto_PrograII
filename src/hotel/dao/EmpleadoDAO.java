/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.dao;

import hotel.entities.Empleado;
import hotel.entities.MiError;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Fabio
 */
public class EmpleadoDAO {

    PuestoDAO pdao = new PuestoDAO();

    /**
     * insertar empleados
     *
     * @param empleado que se va a insertar
     * @return true o false si el empleado está registrado
     */
    public boolean insertar(Empleado empleado) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into empleado(nombre,cedula,id_puesto,contraseña)\n"
                    + "values(?,?,?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, empleado.getNombre());
            stmt.setString(2, empleado.getCedula());
            stmt.setInt(3, empleado.getPuesto().getId());
            stmt.setString(4, empleado.getContrasenna());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo registrar el empleado, favor intente nuevamente");
        }
    }

    /**
     * valida al empleado en el login
     *
     * @param empleado que se va a validar
     * @return true o false si el empleado está validado
     */
    public boolean validar(Empleado empleado) {

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from empleado where cedula = ? and contraseña = ? and activo = true";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, empleado.getCedula());
            stmt.setString(2, empleado.getContrasenna());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                empleado.setNombre(rs.getString("nombre"));
                empleado.setCedula(rs.getString("cedula"));
                empleado.setContrasenna(rs.getString("contraseña"));
                empleado.setId(rs.getInt("id"));
                empleado.setPuesto(pdao.cargarID(rs.getInt("id_puesto")));
                empleado.setActivo(rs.getBoolean("activo"));
                return true;
            }
            sql = "select * from empleado where cedula = ? and activo = true";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, empleado.getCedula());
            rs = stmt.executeQuery();
            if (rs.next()) {
                throw new MiError("Contraseña inválida");
            }
        } catch (SQLException ex) {
            throw new MiError("Problemas al verificar al usuario");
        }
        throw new MiError("Usuario no registrado");
    }

    /**
     * carga el empleado
     *
     * @param rs resultset
     * @return el empleado cargado
     * @throws SQLException
     */
    private Empleado cargarEmpleado(ResultSet rs) throws SQLException {
        Empleado empleado = new Empleado();
        empleado.setId(rs.getInt("id"));
        empleado.setNombre(rs.getString("nombre"));
        empleado.setCedula(rs.getString("cedula"));
        empleado.setContrasenna(rs.getString("contraseña"));

        //Cargar la foranea
        PuestoDAO pdao = new PuestoDAO();
        empleado.setPuesto(pdao.cargarID(rs.getInt("id_puesto")));

        empleado.setActivo(rs.getBoolean("activo"));
        return empleado;
    }

    /**
     * carga el empleado por id
     *
     * @param id del empleado a cargar
     * @return el empleado cargado
     */
    public Empleado cargarID(int id) {
        Empleado empleado = null;

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from empleado where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                empleado = cargarEmpleado(rs);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar los empleados, favor intente nuevamente");
        }

        return empleado;
    }

    /**
     * desactiva al empleado
     *
     * @param id del empleado que se va a desactivar
     * @return true o false si se pudo desactivar el empleado
     */
    public boolean eliminar(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update empleado set activo = false where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo eliminar el empleado, favor intente nuevamente");
        }
    }

    /**
     * cargar todos los activos
     *
     * @return linkedlist con todos los empleados
     */
    public LinkedList<Empleado> cargarTodosActivos() {
        LinkedList<Empleado> empleados = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from empleado";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                empleados.add(cargarEmpleado(rs));
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar los empleados, favor intente nuevamente");
        }

        return empleados;
    }

    /**
     * cargar empleados por filtro
     * @param filtro
     * @return linkedlist con los empleados
     */
    public LinkedList<Empleado> cargarFiltro(String filtro) {
        LinkedList<Empleado> empleados = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from empleado where nombre like ? "
                    + " or cedula like ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, filtro + '%');
            stmt.setString(2, filtro + '%');
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                empleados.add(cargarEmpleado(rs));
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar los empleados, favor intente nuevamente");
        }

        return empleados;
    }
/**
 * editar el empleado
 * @param nuevoEmpleado empleado a editar
 * @return true o false si el empleado se editó
 */
    public boolean editar(Empleado nuevoEmpleado) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update empleado set cedula=?, nombre=?, id_puesto=?,contraseña=?,activo = ?"
                    + " where id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, nuevoEmpleado.getCedula());
            stmt.setString(2, nuevoEmpleado.getNombre());
            stmt.setInt(3, nuevoEmpleado.getPuesto().getId());
            stmt.setString(4, nuevoEmpleado.getContrasenna());
            stmt.setBoolean(5, nuevoEmpleado.isActivo());
            stmt.setInt(6, nuevoEmpleado.getId());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new MiError("No se pudo modificar el empleado, favor intente nuevamente");
        }
    }

}
