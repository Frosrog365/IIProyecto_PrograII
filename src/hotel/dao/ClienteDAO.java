/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.dao;

import hotel.entities.Cliente;
import hotel.entities.MiError;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class ClienteDAO {

    /**
     * Metodo para registrar clientes al sistema
     *
     * @param cliente el cliente que se va a registrar
     * @return true o false si el cliente se registró
     */
    public boolean insertar(Cliente cliente) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into cliente(cuenta,nombre,cedula,telefono_cliente)\n"
                    + "values(?,?,?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, cliente.getCuenta());
            stmt.setString(2, cliente.getNombre());
            stmt.setString(3, cliente.getCedula());
            stmt.setString(4, cliente.getTelefono());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
       
            throw new MiError("No se pudo registrar el cliente, favor intente nuevamente");
        }
    }

    /**
     * cargar cliente por id
     *
     * @param id del cliente
     * @return el cliente cargado
     */
    public Cliente cargarID(int id) {
        Cliente cliente = null;

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from cliente where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                cliente = cargarCliente(rs);
            }
        } catch (Exception ex) {
  
            throw new MiError("Problemas al cargar los clientes, favor intente nuevamente");
        }

        return cliente;
    }

    /**
     * cargar la id del cliente
     *
     * @param c
     * @return true o false si pudo cargar la id
     */
    public boolean cargarID(Cliente c) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from cliente where cedula = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, c.getCedula());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                c.setId(rs.getInt("id"));
                return true;
            }
        } catch (Exception ex) {
    
            throw new MiError("No se pudo cargar el id del cliente, favor intente nuevamente");
        }
        return false;
    }

    /**
     * cargar cliente en el sistema
     *
     * @param rs resultset
     * @return cliente hecho
     * @throws SQLException
     */
    private Cliente cargarCliente(ResultSet rs) throws SQLException {
        Cliente cliente = new Cliente();
        cliente.setId(rs.getInt("id"));
        cliente.setCuenta(rs.getInt("cuenta"));
        cliente.setNombre(rs.getString("nombre"));
        cliente.setCedula(rs.getString("cedula"));
        cliente.setTelefono(rs.getString("telefono_cliente"));

        cliente.setActivo(rs.getBoolean("activo"));
        return cliente;
    }

}
