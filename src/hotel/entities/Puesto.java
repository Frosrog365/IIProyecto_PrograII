/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.entities;

/**
 *
 * @author Fabio_2
 */
public class Puesto {
    private String puesto;
    private int id;
    private boolean activo;

    public Puesto() {
    }

    public Puesto(String puesto, int id,boolean activo) {
        this.puesto = puesto;
        this.id = id;
        this.activo = activo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return puesto;
    }
    
    
}
