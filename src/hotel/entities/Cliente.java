/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.entities;

/**
 *
 * @author Fabio_2
 */
public class Cliente {
    private int cuenta;
    private String nombre;
    private String cedula;
    private String telefono;
    private int id;
    private boolean activo;

    public Cliente() {
    }

    public Cliente(int cuenta, String nombre, String cedula, String telefono, int id, boolean activo) {
        this.cuenta = cuenta;
        this.nombre = nombre;
        this.cedula = cedula;
        this.telefono = telefono;
        this.id = id;
        this.activo = activo;
    }

    public int getCuenta() {
        return cuenta;
    }

    public void setCuenta(int cuenta) {
        this.cuenta = cuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Cliente{" + "cuenta=" + cuenta + ", nombre=" + nombre + ", cedula=" + cedula + ", telefono=" + telefono + ", id=" + id + ", activo=" + activo + '}';
    }
    
    
}
