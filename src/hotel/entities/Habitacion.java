/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.entities;

/**
 *
 * @author Fabio_2
 */
public class Habitacion {
    private String numero;
    private boolean estado;
    private TipoHabitacion tipo;
    private boolean activo;
    private int id;
    private String fechaSalida;

    public Habitacion() {
    }

    public Habitacion(String numero, boolean estado, TipoHabitacion tipo, boolean activo, int id, String fechaSalida) {
        this.numero = numero;
        this.estado = estado;
        this.tipo = tipo;
        this.activo = activo;
        this.id = id;
        this.fechaSalida = fechaSalida;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public TipoHabitacion getTipo() {
        return tipo;
    }

    public void setTipo(TipoHabitacion tipo) {
        this.tipo = tipo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    @Override
    public String toString() {
        return "Habitacion{" + "numero=" + numero + ", estado=" + estado + ", tipo=" + tipo.getTipo() + ", activo=" + activo + ", id=" + id + ", fechaSalida=" + fechaSalida + '}';
    }
    
    
}
