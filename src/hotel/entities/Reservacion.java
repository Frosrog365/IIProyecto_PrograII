/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.entities;

/**
 *
 * @author Fabio_2
 */
public class Reservacion {
    private Cliente cliente;
    private int dias;
    private int desayuno;
    private String fechaEnt;
    private String fechaSal;
    private boolean activo;
    private int id;
    private Habitacion habitacion;

    public Reservacion() {
    }

    public Reservacion(Cliente cliente, int dias, int desayuno, String fechaEnt, String fechaSal, boolean activo, int id, Habitacion habitacion) {
        this.cliente = cliente;
        this.dias = dias;
        this.desayuno = desayuno;
        this.fechaEnt = fechaEnt;
        this.fechaSal = fechaSal;
        this.activo = activo;
        this.id = id;
        this.habitacion = habitacion;
    }

    public Habitacion getHabitacion() {
        return habitacion;
    }

    public void setHabitacion(Habitacion habitacion) {
        this.habitacion = habitacion;
    }
    

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public int getDesayuno() {
        return desayuno;
    }

    public void setDesayuno(int desayuno) {
        this.desayuno = desayuno;
    }

    public String getFechaEnt() {
        return fechaEnt;
    }

    public void setFechaEnt(String fechaEnt) {
        this.fechaEnt = fechaEnt;
    }

    public String getFechaSal() {
        return fechaSal;
    }

    public void setFechaSal(String fechaSal) {
        this.fechaSal = fechaSal;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Reservacion{" + "cliente=" + cliente + ", dias=" + dias + ", desayuno=" + desayuno + ", fechaEnt=" + fechaEnt + ", fechaSal=" + fechaSal + ", activo=" + activo + ", id=" + id + ", habitacion=" + habitacion + '}';
    }
    
    


    
    
    
}
