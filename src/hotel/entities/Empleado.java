/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.entities;

/**
 *
 * @author Fabio_2
 */
public class Empleado {
    private String nombre;
    private String cedula;
    private Puesto puesto;
    private int id;
    private boolean activo;
    private String contrasenna;

    public Empleado() {
    }

    public Empleado(String nombre, String cedula, Puesto puesto, int id, boolean activo, String contrasenna) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.puesto = puesto;
        this.id = id;
        this.activo = activo;
        this.contrasenna = contrasenna;
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    @Override
    public String toString() {
        return nombre+id;
    }
    
    
    
}
