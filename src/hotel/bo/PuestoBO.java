/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.bo;

import hotel.dao.PuestoDAO;
import hotel.entities.Empleado;
import hotel.entities.Puesto;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class PuestoBO {

    private PuestoDAO pdao;

    public PuestoBO() {
        pdao = new PuestoDAO();
    }

    /**
     * Carga los puestos
     *
     * @param activo true carga solo puestos activos, false carga todas los
     * puestos
     * @return LinkedList de puestos
     */
    public LinkedList<Puesto> cargarPuestos(boolean activo) {
        return pdao.selectAll(activo);
    }

    /**
     * carga puestos
     *
     * @return linkedlist de puestos
     */
    public LinkedList<Puesto> cargarPuestos() {
        return pdao.selectAll(false);
    }

    /**
     * carga los puestos
     *
     * @return linkedlist de los puestos
     */
    public LinkedList<Puesto> cargarPuestosActivos() {
        return pdao.selectAll(true);
    }

    /**
     *
     * @param activo para ver si los puestos están activos
     * @param empleado que tiene el puesto
     * @return todos los empleados con puestos
     */
    public LinkedList<Puesto> cargarPuestos(boolean activo, Empleado empleado) {
        LinkedList<Puesto> puestos = pdao.selectAll(activo);
        for (Puesto puesto : puestos) {
            if (empleado == null || puesto.getId() == empleado.getPuesto().getId()) {
                return puestos;
            }
        }
        Puesto t = empleado.getPuesto();
        t.setPuesto(t.getPuesto() + "(actual)");
        puestos.add(t);
        return puestos;
    }

    /**
     * insertar puestos
     *
     * @param p que se va a insertar
     * @return si se pudo insertar puestos o no
     */
    public boolean insertar(Puesto p) {
        PuestoDAO pdao = new PuestoDAO();
        return pdao.insertar(p);
    }

}
