/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.bo;

import hotel.dao.DesayunoDAO;
import hotel.entities.Desayuno;
import java.sql.SQLException;

/**
 *
 * @author Fabio_2
 */
public class DesayunoBO {

    DesayunoDAO ddao = new DesayunoDAO();

    /**
     * Metodo que manda a llamar al insertar de los desayunos
     *
     * @param d el desayuno que se va a insertar
     * @return true o false si el desayuno se insertó
     */
    public boolean insertar(Desayuno d) {
        return ddao.insertar(d);
    }

    /**
     * Metodo para sacar la cantidad de desayunos dependiendo de la fecha
     *
     * @param cantidadDesayunos int a editar
     * @param fecha la fecha solicitada
     * @return la cantidad de desayunos
     * @throws SQLException
     */
    public int cantidadDesayunos(int cantidadDesayunos, String fecha) throws SQLException {
        return ddao.cantidadDesayunos(cantidadDesayunos, fecha);
    }

    public void desactivar(String fecha) {
        ddao.borrar(fecha);
    }
}
