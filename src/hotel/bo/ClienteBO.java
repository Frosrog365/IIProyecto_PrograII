/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.bo;

import hotel.dao.ClienteDAO;
import hotel.entities.Cliente;
import hotel.entities.MiError;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class ClienteBO {

    /**
     * Metodo para registrar clientes al sistema
     *
     * @param cliente el cliente que se va a registrar
     * @return true o false si el cliente se registró
     */
    public boolean registrar(Cliente cliente) {
        if (cliente.getCedula().isEmpty()) {
            throw new MiError("Cédula requerida");
        }
        if (cliente.getCuenta() == -1) {
            throw new MiError("Cuenta requerida/invalida");
        }
        if (cliente.getNombre().isEmpty()) {
            throw new MiError("Nombre requerido");
        }
        ClienteDAO cdao = new ClienteDAO();
        if (cliente.getId() == 0) {
            return cdao.insertar(cliente);
        } else {
            return false;
        }
    }

    /**
     * Metodo para cargar los clientes por ID
     *
     * @param id la id para identificar al cliente
     * @return el cliente cargado
     */
    public Cliente cargarID(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar un cliente");
        }
        ClienteDAO cdao = new ClienteDAO();
        return cdao.cargarID(id);
    }

    /**
     * Metodo para cargar la id del usuario
     *
     * @param c el cliente al que se le va a cargar la ID
     * @return un true o false para ver si se cargó la id del cliente
     */
    public boolean cargarID(Cliente c) {
        ClienteDAO cdao = new ClienteDAO();
        return cdao.cargarID(c);
    }
}
