/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.bo;

import hotel.dao.EmpleadoDAO;
import hotel.entities.Empleado;
import hotel.entities.MiError;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class EmpleadoBO {

    /**
     * Metodo para llamar al registro de empleados
     *
     * @param empleado el empleado que hay que registrar
     * @return true o false si pudo registrar el empleado
     */
    public boolean registrar(Empleado empleado) {
        if (empleado.getCedula().isEmpty()) {
            throw new MiError("Cédula requerida");
        }
        if (empleado.getContrasenna().isEmpty()) {
            throw new MiError("Contraseña requerida");
        }
        EmpleadoDAO edao = new EmpleadoDAO();
        System.out.println(MD5(empleado.getContrasenna()));
        empleado.setContrasenna(MD5(empleado.getContrasenna()));
        if (empleado.getId() == 0) {
            return edao.insertar(empleado);
        } else {
            return false;
        }
    }

    /**
     * Metodo para cargar a los empleados
     *
     * @param filtro filtro que se usa para ver ls empleados que se necesitan
     * @return una lista de empleados
     */
    public LinkedList<Empleado> cargarTodo(String filtro) {
        EmpleadoDAO udao = new EmpleadoDAO();
        if (filtro.isEmpty()) {
            return udao.cargarTodosActivos();
        } else {
            return udao.cargarFiltro(filtro);
        }
    }

    /**
     * Metodo que se usa para verificar al usuario en el login
     *
     * @param empleado El empleado para autentificar
     * @return true o false de si el empleado entra
     */
    public boolean autenficar(Empleado empleado) {
        if (empleado.getCedula().isEmpty()) {
            throw new MiError("Cédula requerida");
        }

        if (empleado.getContrasenna().isEmpty()) {
            throw new MiError("Contraseña requerida");
        }
        EmpleadoDAO edao = new EmpleadoDAO();
        empleado.setContrasenna(MD5(empleado.getContrasenna()));
        return edao.validar(empleado);
    }

    /**
     * Metodo para encriptar la contraseña
     *
     * @param md5 el string que se va a encriptar
     * @return el string encriptado
     */
    private String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    /**
     * Cargar el empleado por ID
     *
     * @param id el id del empleado
     * @return el empleado
     */
    public Empleado cargarID(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar un usuario");
        }
        EmpleadoDAO udao = new EmpleadoDAO();
        return udao.cargarID(id);
    }

    /**
     * Desactivar al empleado
     *
     * @param id el id del empleado que se va a borrar
     * @return true o false si se pudo borrar el empleado
     */
    public boolean borrar(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar un usuario");
        }
        EmpleadoDAO udao = new EmpleadoDAO();
        return udao.eliminar(id);
    }

    /**
     * editar al empleado
     *
     * @param nuevoEmpleado que se va a editar
     * @return true o false si el empleado se pudo editar
     */
    public boolean editar(Empleado nuevoEmpleado) {
        EmpleadoDAO edao = new EmpleadoDAO();
        nuevoEmpleado.setContrasenna(MD5(nuevoEmpleado.getContrasenna()));
        return edao.editar(nuevoEmpleado);
    }
}
