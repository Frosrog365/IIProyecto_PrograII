/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.bo;

import hotel.dao.HabitacionDAO;
import hotel.entities.Habitacion;
import hotel.entities.MiError;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class HabitacionBO {

    /**
     * Cargar las habitaciones
     *
     * @param fecha fecha para cargar la habitacion
     * @return una lista de habitaciones
     */
    public LinkedList<Habitacion> cargarTodo(String fecha) {
        HabitacionDAO hdao = new HabitacionDAO();
        if (fecha.isEmpty()) {
            return null;
        } else {
            return hdao.cargarFiltro(fecha);
        }
    }

    /**
     * cargar todas las habitaciones activas
     *
     * @return una lista de habitaciones
     */
    public LinkedList<Habitacion> cargarTodosActivos() {
        HabitacionDAO hdao = new HabitacionDAO();
        return hdao.cargarTodosActivos();
    }

    /**
     * cargar la habitacion por id
     *
     * @param id de la habitacion
     * @return la habitacion por id
     */
    public Habitacion cargarID(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar un usuario");
        }
        HabitacionDAO udao = new HabitacionDAO();
        return udao.cargarID(id);
    }

    /**
     * insertar la habitacion
     *
     * @param h la habitacion que se va a insertar
     * @return true o false si la habitaicion se insertó
     */
    public boolean insertar(Habitacion h) {
        HabitacionDAO hdao = new HabitacionDAO();
        return hdao.insertar(h);
    }

    /**
     * ocupar la habitaicon
     *
     * @param habitacion que se va a ocupar
     * @return true o false si la habitacion se ocupó
     */
    public boolean ocupar(Habitacion habitacion) {
        HabitacionDAO hdao = new HabitacionDAO();
        return hdao.ocupar(habitacion);
    }

    /**
     * desocupar la habitacion
     *
     * @param habitacion que se va a desocupar
     * @return true o false si la habitacion se desocupo
     */
    public boolean desocupar(Habitacion habitacion) {
        HabitacionDAO hdao = new HabitacionDAO();
        habitacion.setEstado(false);
        habitacion.setFechaSalida("");
        return hdao.desocupar(habitacion);
    }

}
