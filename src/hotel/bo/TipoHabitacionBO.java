/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.bo;

import hotel.dao.TipoHabitacionDAO;
import hotel.entities.Habitacion;
import hotel.entities.TipoHabitacion;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class TipoHabitacionBO {
    private TipoHabitacionDAO tdao;

    
    public TipoHabitacionBO() {
        this.tdao = new TipoHabitacionDAO();
    }
    
/**
 * carga tipos de habitacion y los asigna
 * @param b true o false si está activo o no
 * @param habitacion que se le asigna el tipo
 * @return lista con habitaciones cargadas
 */
    public LinkedList<TipoHabitacion> cargarTipos(boolean b, Habitacion habitacion) {
        LinkedList<TipoHabitacion> tipos = tdao.selectAll(b);
        for (TipoHabitacion tipo : tipos) {
            if (habitacion == null || tipo.getId() == habitacion.getTipo().getId()) {
                return tipos;
            }
        }
        TipoHabitacion tipo = habitacion.getTipo();
        tipo.setTipo(tipo.getTipo()+ "(actual)");
        tipos.add(tipo);
        return tipos;
    }
/**
 * insertar habitacion
 * @param t habitacion que se inserta
 * @return true o false si se insertó la habitacion
 */
    public boolean insertar(TipoHabitacion t) {
        TipoHabitacionDAO tdao = new TipoHabitacionDAO();
        return tdao.insertar(t);
    }

}
