/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.bo;

import hotel.dao.ReservacionDAO;
import hotel.entities.MiError;
import hotel.entities.Reservacion;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class ReservacionBO {

    /**
     * registrar una reservacion
     * @param reservacion que se va a registrar
     * @return true o false si se pudo registrar
     */
    public boolean registrar(Reservacion reservacion) {
        if (reservacion.getDias()==-1) {
            throw new MiError("Días requeridos");
        }

        if (reservacion.getFechaEnt().isEmpty()) {
            throw new MiError("Fecha de entrada requerida");
        }
        if (reservacion.getFechaSal().isEmpty()) {
            throw new MiError("Fecha de salida requerida");
        }
        if(reservacion.getHabitacion()==null){
            throw new MiError("Debe escoger al menos una habitacion");
        }

        ReservacionDAO rdao = new ReservacionDAO();
        if (reservacion.getId() == 0) {
            return rdao.insertar(reservacion);
        } else {
            return false;
        }
    }
/**
 * cargar las reservaciones
 * @param filtro que se usa para revisar las reservaciones 
 * @return linkedlist con las reservaciones
 */
    public LinkedList<Reservacion> cargarTodo(String filtro) {
        ReservacionDAO rdao = new ReservacionDAO();
        if (filtro.isEmpty()) {
            return rdao.cargarTodosActivos();
        } else {
            return rdao.cargarFiltro(filtro);
        }
    }
    /**
     * Cargar reservacion por ID
     * @param id de la reservacion
     * @return reservacion cargada
     */
    public Reservacion cargarID(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar una reservacion");
        }
        ReservacionDAO rdao = new ReservacionDAO();
        return rdao.cargarID(id);
    }
/**
 * borrar las reservaciones
 * @param id de la reservacion
 * @return true o false si se borró la reservacion
 */
    public boolean borrar(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar una reservacion");
        }
        ReservacionDAO rdao = new ReservacionDAO();
        return rdao.borrar(id);
    }
    public boolean editar(Reservacion r){
        ReservacionDAO rdao = new ReservacionDAO();
        return rdao.editar(r);
    }
}
